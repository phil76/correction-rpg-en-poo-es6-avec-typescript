import Character from '../character';
import Enemy from '../enemies/enemy';
import Race from './race';
import ICharacterConfig from '../models/ICharacterConfig';
import { EnumRace } from '../constant/enumRace';
import IBonus from '../models/IBonus';

/**
 * Class Hero comportant 3 race (humans, elf, dwarf)
 * @extends Character
 */
export default class Hero extends Character
{
    race: Race;
    constructor(config: ICharacterConfig, raceType: EnumRace)
    {
        super(config);
        this.race = new Race(raceType);
    }

    /**
     * Recupere la race du Hero
     */
    getRace(): Race
    {
        return this.race;
    }

    /**
     * Assigne la race du Hero
     * @param raceType 
     */
    setRace(raceType: EnumRace): void
    {
        this.race = new Race(raceType);
    }

    /**
     * Assigne des degat au Hero
     * @param damage 
     * @param enemy 
     */
    setDamage(damage: number, enemy: Enemy): void
    {
        // Si le Dwarf annule 50% de degat
        if (this.race.getRace() === EnumRace.Dwarf)
        {
            const defenseBonus = this.race.getRaceBonus(enemy);

            if (defenseBonus.defense === 50)
            {
                this.health -= damage / 2;
                console.log(this.getName() + ' a esquiver 50% des degats...Bravo!!')
                return;
            }
        }
        // Sinon on applique les degat normaux pour Human Elf et Dwarf
        this.health -= damage;
    }


    /**
     * Attaque un ennemi
     * @param enemy 
     */
    attack(enemy: Enemy): number
    {
        const damageBonus: IBonus = this.race.getRaceBonus(enemy);

        if (damageBonus.attack)
        {
            return (this.hitStrength + this.hitStrength * 0.1) * this.level;
        }

        return (this.hitStrength) * this.level;
    }

    /**
     * Le Hero est mort...
     */
    die()
    {
        console.log(this.getName() + ' est mort... GAME OVER')
    }
}
