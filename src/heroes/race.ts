import Enemy from "../enemies/enemy";
import IBonus from "../models/IBonus";
import { EnumRace } from "../constant/enumRace";

/**
 * Class Race de Hero
 */
export default class Race
{
    race: EnumRace;
    constructor(typeOfRace: EnumRace)
    {
        this.race = typeOfRace;
    }

    /**
     * Recupere la race du heros
     */
    getRace()
    {
        return this.race;
    }

    /**
     * Assigne une race au heros
     * @param race 
     */
    setRace(race: EnumRace)
    {
        this.race = race;
    }

    /**
     * Recupere les bonus d'attaque ou defense en fonction
     * de la race du Hero et de l'ennemi
     * @param enemy 
     */
    getRaceBonus(enemy: Enemy | undefined): IBonus
    {
        // Humain contre ennemi au sol
        if (this.race === EnumRace.Human && !enemy.canFly)
        {
            return { attack: true, defense: 0 };
        }

        // Humain contre ennemi volant
        if (this.race === EnumRace.Human && enemy.canFly)
        {
            return { attack: false, defense: 0 };
        }

        // Elf contre ennemi au sol
        if (this.race === EnumRace.Elf && !enemy.canFly)
        {
            return { attack: false, defense: 0 };
        }

        // Elf contre ennemi volant
        if (this.race === EnumRace.Elf && enemy.canFly)
        {
            return { attack: true, defense: 0 };
        }

        // Nain contre ennemi
        if (this.race === EnumRace.Dwarf)
        {
            const randomBonus = Math.floor(Math.random() * 100);

            if (randomBonus > 20)
            {
                return { attack: false, defense: 0 };
            }
            else
            {
                return { attack: false, defense: 50 };
            }
        }
    }
}