import IBattleConfig from '../models/IBattleConfig';

/**
 * Fichier de config par defaut du hero et des ennemis
 */
export const battleConfig = {
    hero: {
        name: 'Arthur',
        health: 300,
        hitStrength: 25,
        level: 1,
        xp: 0
    },
    assassin: {
        name: 'Desmond',
        health: 50,
        hitStrength: 10,
        level: 1,
        xp: 0
    },
    berserker: {
        name: 'Rek',
        health: 50,
        hitStrength: 5,
        level: 1,
        xp: 0
    },
    dragon: {
        name: 'Smaug',
        health: 140,
        hitStrength: 20,
        level: 1,
        xp: 0
    },
    golem: {
        name: 'Melog',
        health: 30,
        hitStrength: 10,
        level: 1,
        xp: 0
    },
    griffin: {
        name: 'Kigrif',
        health: 30,
        hitStrength: 10,
        level: 1,
        xp: 0
    },
    werewolf: {
        name: 'Lyca',
        health: 30,
        hitStrength: 10,
        level: 1,
        xp: 0
    },
} as IBattleConfig;
