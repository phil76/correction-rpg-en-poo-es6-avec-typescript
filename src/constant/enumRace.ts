/**
 * Dictionnaire permettant de referencer 
 * les races disponibles pour le Hero
 */
export enum EnumRace {
    Human = 'Human',
    Elf = 'Elf',
    Dwarf = 'Dwarf'
}