import Enemy from "../enemies/enemy";
import Hero from "../heroes/hero";

/**
 * Class permettant d'ajouter des lignes sur les tables HTML
 */
export default class Stat
{
    table: HTMLTableElement;
    constructor(tableName: string)
    {
        this.table = document.getElementById(tableName) as HTMLTableElement;
    }

    /**
     * Ajoute une ligne de stat au tableau
     * @param statConfig 
     * @param round 
     */
    async insertRoundRow(statConfig: (Hero | Enemy)[], round: number)
    {
        const rowCount = this.table.rows.length;

        const row = this.table.insertRow(rowCount);

        row.innerHTML = `
        <tr>
            <th colspan="1" class="red">${round}</th>
            <th colspan="1">${statConfig[0].getName()}</th>
            <th colspan="1">${statConfig[0].getHealth()}</th>
            <th colspan="1">${statConfig[0].getHitStrength()}</th>
            <th colspan="1">${statConfig[0].getLvl()}</th>
            <th colspan="1">${statConfig[0].getXp()}</th>
            <th colspan="1" class="red">VS</th>
            <th colspan="1">${statConfig[1].getName()}</th>
            <th colspan="1">${statConfig[1].getHealth()}</th>
            <th colspan="1">${statConfig[1].getHitStrength()}</th>
            <th colspan="1">${statConfig[1].getLvl()}</th>
            <th colspan="1">${statConfig[1].getXp()}</th>
        </tr>`;
    }

    /**
     * Ajoute une ligne de séparation au tableau
     */
    async insertBattleEndRow()
    {
        const rowCount = this.table.rows.length;

        const row = this.table.insertRow(rowCount);

        row.innerHTML = `
        <tr>
            <th colspan="6"></th>
            <th colspan="1">---</th>
            <th colspan="5"></th>
        </tr>`;
    }
}