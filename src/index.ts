import BattleSimulation from './battles/battleSimulation';
import { battleConfig } from './constant/battleConfig';


const batSim = new BattleSimulation();

// On cree un Hero a partir du fichier battleConfig
batSim.createHero(battleConfig);

// On cree un groupe d'ennemis a partir du fichier battleConfig
batSim.createEnemies(battleConfig);

// on nettoie la console des log precedents
console.clear();

console.log('*** HERO VS ENEMIES ***');
batSim.startHeroVsEnemies();

console.log('*** ENEMY VS ENEMIES ***');
batSim.startEnemyVsEnemies(0);
