import Enemy from './enemy';
import ICharacterConfig from '../models/ICharacterConfig';

/**
 * Class Golem
 * @extends Enemy
 */
export default class Golem extends Enemy
{
    constructor(config: ICharacterConfig)
    {
        super(config);
    }

    /**
     * Le Golem a un setHealth qui surcharge celui de Character
     * car il a 50% de chance d'annuler les degats
     * @param nbr 
     */
    setDamage(nbr: number)
    {
        const randomDefense = Math.floor(Math.random() * 100);

        if (randomDefense < 50)
        {
            this.health -= Math.floor(nbr);
        }
        else
        {
            console.log('Mais ' + this.getName() + ' a esquiver les degats...dommage!!')
        }
    }
}
