import Enemy from './enemy';
import ICharacterConfig from '../models/ICharacterConfig';

/**
 * Class Assassin
 * @extends Enemy
 */
export default class Assassin extends Enemy
{
    attackCount: number = -0.1;
    constructor(config: ICharacterConfig)
    {
        super(config);
    }

    /**
     * L'Assassin a un attack qui surcharge celui de Character
     * car il augmente son hitStrength de 10% a chaque nouvelle attaque
     * @param nbr 
     */
    attack(): number
    {
        this.attackCount += 0.1;
        return (this.hitStrength + this.hitStrength * this.attackCount) * this.level;
    }
}
