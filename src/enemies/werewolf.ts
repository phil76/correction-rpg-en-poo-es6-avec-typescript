import Enemy from './enemy';
import ICharacterConfig from '../models/ICharacterConfig';

/**
 * Class Werewolf
 * @extends Enemy
 */
export default class Werewolf extends Enemy
{
    constructor(config: ICharacterConfig)
    {
        super(config);
    }

    /**
     * Le Werewolf a un setHealth qui surcharge celui de Character
     *  car il a une resistance de 50% par default
     * @param nbr 
     */
    setDamage(nbr: number)
    {
        this.health -= Math.floor(nbr * 0.5);
        console.log(this.getName() + ' a annulé 50% des degats');
    }
}
