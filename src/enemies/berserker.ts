import Enemy from './enemy';
import ICharacterConfig from '../models/ICharacterConfig';

/**
 * Class Berserker
 * @extends Enemy
 */
export default class Berserker extends Enemy
{
    constructor(config: ICharacterConfig)
    {
        super(config);
    }

    /**
     * Le Berserker a un setHealth qui surcharge celui de Character
     *  car il a une resistance de 30% par default
     * @param nbr 
     */
    setDamage(nbr: number)
    {
        this.health -= Math.floor(nbr * 0.7);
        console.log(this.getName() + ' a annulé 30% des degats');
    }
}
