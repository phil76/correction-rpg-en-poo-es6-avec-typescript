import Enemy from "./enemy";
import ICharacterConfig from "../models/ICharacterConfig";

/**
 * Class Griffin
 * @extends Enemy
 */
export default class Griffin extends Enemy
{
    isFlying: boolean = false;
    startFlying: boolean = false;
    constructor(config: ICharacterConfig)
    {
        super(config);

        this.canFly = true;
    }

    /**
     * Le Griffin a un setHealth qui surcharge celui de Character
     *  car il a une resistance de 50% par default
     *  + 10% s'il est en l'air
     * @param nbr 
     */
    setDamage(nbr: number)
    {
        this.health -= Math.floor(nbr * (this.isFlying ? 0.6 : 0.5));
    }

    /**
     * Le Griffin a un attack qui surcharge celui de Character
     * car il augmente de 10% son attaque lorsqu'il est en l'air
     * ou n'attaque pas s'il décolle
     */
    attack()
    {
        // Le Griffin décolle
        if (this.startFlying)
        {
            this.isFlying = true;
            this.startFlying = false;
            console.log(this.getName() + ' prend son envol');
            return 0;
        }

        // Le Griffin attaque du sol
        if (!this.isFlying)
        {
            this.startFlying = true;
            return this.hitStrength * this.level;
        }

        // Le Griffin attaque du ciel
        if (this.isFlying)
        {
            this.isFlying = false;
            console.log(this.getName() + ' attaque du ciel avec une attaque de ' + (this.hitStrength + this.hitStrength * 0.1) * this.level);
            return (this.hitStrength + this.hitStrength * 0.1) * this.level;
        }
    }
}
