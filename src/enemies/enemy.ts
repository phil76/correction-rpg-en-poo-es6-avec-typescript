import Character from '../character';
import ICharacterConfig from '../models/ICharacterConfig';

/**
 * Class Enemy de base
 * @extends Character
 */
export default class Enemy extends Character
{
    canFly: boolean = false;
    constructor(config: ICharacterConfig)
    {
        super(config);
    }

    setDamage(nbr: number): void
    {
        this.health -= Math.floor(nbr);
    }
}
