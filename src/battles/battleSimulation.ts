import Battle from './battle';
import Hero from '../heroes/hero';
import Enemy from '../enemies/enemy';
import Assassin from '../enemies/assassin';
import Berserker from '../enemies/berserker';
import Dragon from '../enemies/dragon';
import Golem from '../enemies/golem';
import Griffin from '../enemies/griffin';
import Werewolf from '../enemies/werewolf';
import { EnumRace } from '../constant/enumRace';
import { battleConfig } from '../constant/battleConfig';
import IBattleConfig from '../models/IBattleConfig';


/**
 * Lance des simulations de combats
 */
export default class BattleSimulation
{
    enemyGroup: Enemy[] = [];
    hero: Hero;
    assassin: Assassin;
    berserker: Berserker;
    dragon: Dragon;
    golem: Golem;
    griffin: Griffin;
    werewolf: Werewolf;
    counter: number = 0;
    
    /**
     * Crée un Hero
     */
    createHero(battleConfig: IBattleConfig)
    {
        this.hero = new Hero(battleConfig.hero, EnumRace.Elf);
    }

    /**
     * Crée un groupe d'ennemis
     */
    createEnemies(battleConfig: IBattleConfig)
    {
        this.assassin = new Assassin(battleConfig.assassin);
        this.berserker = new Berserker(battleConfig.berserker);
        this.dragon = new Dragon(battleConfig.dragon);
        this.golem = new Golem(battleConfig.golem);
        this.griffin = new Griffin(battleConfig.griffin);
        this.werewolf = new Werewolf(battleConfig.werewolf);

        this.enemyGroup.push(this.werewolf, this.griffin, this.golem, this.assassin, this.berserker, this.dragon);
    }

    /**
     * Faire combattre le Hero contre chaque Enemy
     * jusqu'à la mort du Hero 
     */
    startHeroVsEnemies()
    {
        for (let i = 0; i < this.enemyGroup.length; i += 1)
        {
            // on crée une battle
            const battle = new Battle(this.hero, this.enemyGroup[i]);
            // et on la lance
            battle.startBattle();

            if (this.hero.getHealth() < 0)
            {
                return;
            }
        }

        if (this.hero.getHealth() > 0)
        {
            this.startHeroVsEnemies();
        }
    }

    /**
     * Fait combattre un Enemy contre tout les autres Enemy
     * @param index 
     */
    startEnemyVsEnemies(index: number)
    {
        // On reset le groupe d'ennemis
        this.enemyGroup.length = 0;

        // On cree un nouveau groupe d'ennemis par defaut
        this.createEnemies(battleConfig);

        // On definit l'ennemi principal qui va combattre les autres
        let mainEnemy = this.enemyGroup[this.counter];

        // On stocke la vie de l'ennemi dans une variable
        const mainEnemyHealth = mainEnemy.getHealth();

        // On enleve l'ennemi principal de enemyGroup, il va pas se combattre lui meme
        this.enemyGroup.splice(this.counter, 1);

        console.log('########################################################');
        console.log(`----- ${mainEnemy.getName()} affronte les autres ennemis -----`);
        

        // Le combat commence
        for (let i = 0; i < this.enemyGroup.length; i += 1)
        {
            // On remet la valeur par defaut de health de l'ennemi principal
            mainEnemy.setHealth(mainEnemyHealth);

            // On crée une Battle
            const battle = new Battle(mainEnemy, this.enemyGroup[i]);
            // et on la lance
            battle.startBattle();
        }

        // On incremente counter pour definir le heros suivant
        this.counter += 1;

        // on verifie qu'il reste des combattants sinon on quitte la fonction avec return
        if (this.counter === 6)
        {
            return;
        }

        // et on relance la bataille
        this.startEnemyVsEnemies(this.counter);
    }
}