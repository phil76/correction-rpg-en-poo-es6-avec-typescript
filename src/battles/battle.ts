import Enemy from "../enemies/enemy";
import Hero from "../heroes/hero";
import Stat from "../stats/stat";


export default class Battle
{
    fighterA: Hero | Enemy;
    fighterB: Enemy;
    round: number = 0;
    constructor(fighterA: Hero | Enemy, fighterB: Enemy)
    {
        this.fighterA = fighterA;
        this.fighterB = fighterB;
    }

    /**
     * Demarre les combats
     */
    startBattle()
    {
        // Battle Hero VS Enemy
        if (this.fighterA instanceof Hero)
        {
            this.startBattleHeroVsEnemy(this.fighterA, this.fighterB)
        }

        // Battle Enemy VS Enemy
        if (this.fighterA instanceof Enemy)
        {
            this.startBattleEnemyVsEnemy(this.fighterA, this.fighterB)
        }
    }

    /**
     * Combat entre Hero et Enemy
     * @param fighterA 
     * @param fighterB 
     */
    startBattleHeroVsEnemy(fighterA: Hero, fighterB: Enemy)
    {
        console.log('--------------------------------------------');
        const enemyHealthRecompense: number = fighterB.getHealth();

        while (this.fighterA.getHealth() > 0 && this.fighterB.getHealth() > 0)
        {
            if (this.round % 2 === 0)
            {
                // Le heros attaque l'ennemi
                const damage: number = fighterA.attack(fighterB);
                console.log(fighterA.getName() + ' lvl ' + fighterA.getLvl() + ' inflige ' + damage + ' de degat');
                fighterB.setDamage(damage);

                // On ajoute une nouvelle ligne au tableau de stat
                const stat = new Stat('heroBattle');
                stat.insertRoundRow([fighterA, fighterB], this.round);

                // on incremente le round
                this.round += 1;
            }
            else
            {
                // L'ennemi attaque le heros
                const damage: number = fighterB.attack(fighterA);
                console.log(fighterB.getName() + ' lvl ' + fighterB.getLvl() + ' inflige ' + damage + ' de degat');
                fighterA.setDamage(damage, fighterB);

                // On ajoute une nouvelle ligne au tableau de stat
                const stat = new Stat('heroBattle');
                stat.insertRoundRow([fighterA, fighterB], this.round);

                // on incremente le round
                this.round += 1;
            }
        }

        // Le heros recupere 10% de vie de l'ennemi s'il a survecu
        if (this.fighterA.getHealth() > 0)
        {
            const heroActualHealth: number = this.fighterA.getHealth();
            this.fighterA.setHealth(heroActualHealth + enemyHealthRecompense / 10);
        }

        // Les ennemis mort incrementent leur level de 1 pour un futur combat
        if (this.fighterB.getHealth() <= 0)
        {
            this.fighterB.setLvl();
            // on leur remet la vie par default x2 pour le prochain combat
            this.fighterB.setHealth(enemyHealthRecompense * 2);
        }

        // on reset les round pour le prochain combat
        this.round = 0

        // On rajoute une ligne de separation dans le tableau
        const stat = new Stat('heroBattle');
        stat.insertBattleEndRow();

        // on declare le survivant vainqueur
        this.declareWinner();
    }

    /**
     * Combat Enemy contre Enemy
     * @param fighterA 
     * @param fighterB 
     */
    startBattleEnemyVsEnemy(fighterA: Enemy, fighterB: Enemy)
    {
        console.log('--------------------------------------------');

        while (this.fighterA.getHealth() > 0 && this.fighterB.getHealth() > 0)
        {
            if (this.round % 2 === 0)
            {
                // Le heros attaque l'ennemi
                const damage: number = fighterA.attack(fighterB);
                console.log(fighterA.getName() + ' lvl ' + fighterA.getLvl() + ' inflige ' + damage + ' de degat');
                fighterB.setDamage(damage);

                // On ajoute une nouvelle ligne au tableau de stat
                const stat = new Stat('enemyBattle');
                stat.insertRoundRow([fighterA, fighterB], this.round);

                // on incremente le round
                this.round += 1;
            }
            else
            {
                // L'ennemi attaque le heros
                const damage: number = fighterB.attack(fighterA);
                console.log(fighterB.getName() + ' lvl ' + fighterB.getLvl() + ' inflige ' + damage + ' de degat');
                fighterA.setDamage(damage);

                // On ajoute une nouvelle ligne au tableau de stat
                const stat = new Stat('enemyBattle');
                stat.insertRoundRow([fighterA, fighterB], this.round);

                // on incremente le round
                this.round += 1;
            }
        }

        // On rajoute une ligne de separation dans le tableau
        const stat = new Stat('enemyBattle');
        stat.insertBattleEndRow();

        // on declare le survivant vainqueur
        this.declareWinner();
    }

    /**
     * Déclare le vainqueur
     */
    declareWinner(): Hero | Enemy
    {
        if (this.fighterA.getHealth() > 0)
        {
            this.fighterB.die();
            this.fighterA.setXp();

            console.log(`${this.fighterA.getName()} gagne contre ${this.fighterB.getName()} avec ${this.fighterA.getHealth()} de pv restants`);
            return this.fighterA;
        }

        if (this.fighterB.getHealth() > 0)
        {
            this.fighterA.die();
            this.fighterB.setXp();

            console.log(`${this.fighterB.getName()} gagne contre ${this.fighterA.getName()} avec ${this.fighterB.getHealth()} de pv restants`);
            return this.fighterB;
        }
    }
}
