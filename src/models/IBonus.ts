/**
 * Permet de definir un type IBonus
 */
export default interface IBonus
{
    attack: boolean,
    defense: number
}