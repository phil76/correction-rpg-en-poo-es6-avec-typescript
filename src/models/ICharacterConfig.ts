/**
 * Permet de définir un type pour les config de Character
 */
export default interface ICharacterConfig
{
    name: string,
    health: number,
    hitStrength: number,
    level: number,
    xp: number,
}