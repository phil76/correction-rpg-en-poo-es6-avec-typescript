import ICharacterConfig from "./ICharacterConfig";

/**
 * Permet de definir un type IBattleConfig
 */
export default interface IBattleConfig
{
    hero: ICharacterConfig,
    assassin: ICharacterConfig,
    berserker: ICharacterConfig,
    dragon: ICharacterConfig,
    golem: ICharacterConfig,
    griffin: ICharacterConfig,
    werewolf: ICharacterConfig,
}
