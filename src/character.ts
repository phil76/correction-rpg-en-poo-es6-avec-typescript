import ICharacterConfig from "./models/ICharacterConfig";

/**
 * Class Character avec les propriétés communes
 * au heros et aux ennemis
 * @param config de type ICharacterConfig
 */
export default class Character
{
    name: string;
    health: number;
    hitStrength: number;
    level: number;
    xp: number;
    constructor(config: ICharacterConfig)
    {
        this.name = config.name;
        this.health = config.health;
        this.hitStrength = config.hitStrength;
        this.level = config.level;
        this.xp = config.xp;
    }

    /**
     * Recupere le nom du Character
     */
    getName(): string
    {
        return this.name;
    }

    /**
     * 
     * @param str Assigne le nom du Character
     */
    setName(str: string): void
    {
        this.name = str;
    }

    /**
     * Recupere la vie du Character
     */
    getHealth(): number
    {
        return this.health;
    }

    /**
     * Assigne la vie du Character
     * @param nbr 
     */
    setHealth(nbr: number): void
    {
        this.health = Math.floor(nbr);
    }

    /**
     * Recupere le hitStrength du Character
     */
    getHitStrength(): number
    {
        return this.hitStrength;
    }

    /**
     * Assigne le hitStrength du Character
     * @param nbr 
     */
    setHitStrength(nbr: number): void
    {
        this.hitStrength = Math.floor(nbr);
    }

    /**
     * Recupere le level du Character
     */
    getLvl(): number
    {
        return this.level;
    }

    /**
     * Assigne le level du Character
     * @param nbr Assigne le level du Character
     */
    setLvl(): void
    {
        this.level += 1;
    }

    /**
     * Recupere l'xp du Character
     */
    getXp(): number
    {
        return this.xp;
    }

    /**
     * Assigne l'xp du Character,
     * et augmente son level si égal à 10
     */
    setXp(): void
    {
        this.xp += 2;

        if (this.xp === 10)
        {
            this.setLvl();
            this.xp = 0;
        }
    }

    /**
     * Attaque de base
     */
    attack(enemy: Character): number
    {
        return this.hitStrength * this.level;
    }

    /**
     * Le Character est mort
     */
    die(): void
    {
        console.log(`${this.name} est mort`);
    }
}
